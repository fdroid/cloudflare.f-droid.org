import { CacheKey } from './utils/cacheKey'
import { parseAcceptLanguage } from 'intl-parse-accept-language'

const WEBSITE_URL = new URL('https://f-droid.org')
const WEBSITE_STAGING_URL = WEBSITE_URL
const ORIGIN_PATH = ''
const WEBSITE_ORIGIN_RESOLVE_OVERRIDE = 'origin.f-droid.org'

const REPO_MIRRORS = new Map<string, URL>([
  ['us', new URL('https://plug-mirror.rcac.purdue.edu/fdroid')],
  ['taiwan', new URL('https://mirror.ossplanet.net/fdroid')],
  ['sweden', new URL('https://ftp.lysator.liu.se/pub/fdroid')],
  ['france', new URL('https://fdroid.tetaneutral.net/fdroid')],
])
// use a mirror whose `Etag` is recignized by cloudflare as a strong etag,
// to preserve the `Accept-Ranges` header, and thus the progress indicator in the clients
const DEFAULT_REPO_MIRROR = REPO_MIRRORS.get('us')!

const SEARCH_ORIGIN = new URL('https://search.f-droid.org')
const SEARCH_PATH_REGEX = /^\/search/

const INDEX_PATH_REGEX = /^\/repo\/index(-v1)?\.(jar|xml)/

const CONTENT_TYPES = new Map<string, string>([
  ['.apk', 'application/vnd.android.package-archive'],
  ['.gz', 'application/gzip'],
  ['.asc', 'application/pgp-signature'],
  ['.jar', 'application/java-archive'],
])
const SUFFIX_REGEX = /\.\w+$/

const defaultCfProperties: RequestInitCfProperties = {
  // Always cache this fetch regardless of content type
  // Force response to be cached for 31536000 seconds for 200 status// codes, 1 second for 404, and do not cache 500 errors.
  // Only available for business plan
  // cacheTtlByStatus: { '200-301': 31536000, 404: 1, '500-599': 0 },
  cacheEverything: true,
  minify: {
    javascript: true,
    css: true,
    html: true,
  },
}

declare let MEILISEARCH_DOCS_SEARCHBAR_VERSION: string
declare let MEILISEARCH_HOST_URL: string
declare let MEILISEARCH_API_KEY: string

declare let ENVIRONMENT: string

async function getSupportedLanguages(): Promise<string[]> {
  const resp = await fetch(
    'https://gitlab.com/fdroid/fdroid-website/-/raw/master/_data/languagenames.json',
  )
  const languages = Object.keys(await resp.json())

  return languages
}

function getMirror(continent: string, country: string, colo: string): URL {
  // switch (country) {
  //   // case "DE":
  //   //   return new URL("https://ftp.fau.de/fdroid")
  //   // case "FR":
  //   // case "GB":
  //   //   return new URL("https://f-droid.org");
  //   case 'SE':
  //     return new URL('https://ftp.lysator.liu.se/pub/fdroid')
  //   default:
  //     break
  // }

  // All China users will be routed to LAX ingress servers but they are actually in Asia
  // Should select the closest origin server to the ingress server
  let mirror: URL
  if (colo === 'LAX') {
    mirror = REPO_MIRRORS.get('us') || DEFAULT_REPO_MIRROR
  } else {
    // AF Africa
    // NA North America
    // OC Oceania
    // AN Antarctica
    // AS Asia
    // EU Europe
    // SA South America
    switch (continent) {
      // case 'AS':
      // case 'OC':
      //   return new URL(
      //     REPO_MIRRORS.get('taiwan')?.toString() || DEFAULT_REPO_MIRROR,
      //   )
      // case 'NA':
      // case 'SA':
      //   mirror = REPO_MIRRORS.get('us') || DEFAULT_REPO_MIRROR
      //   break
      // case 'AF':
      // case "EU":
      //   return new URL('https://ftp.lysator.liu.se/pub/fdroid')
      default:
        mirror = DEFAULT_REPO_MIRROR
        break
    }
  }
  return new URL(mirror.toString())
}

export async function handleRequest(request: Request): Promise<Response> {
  const url = new URL(request.url)
  const workerURL = url

  let response: Response
  const language = parseAcceptLanguage(
    request.headers.get('accept-language') || '',
  )[0]

  const cacheKeyHandler = new CacheKey(url, language)
  const cacheKey = cacheKeyHandler.toString()
  console.debug(cacheKey)

  if (url.pathname === '/repo' || url.pathname.startsWith('/repo/')) {
    response = await handleRepo(url, request, cacheKey, workerURL)
  } else if (url.pathname.match(SEARCH_PATH_REGEX)) {
    response = await handleSearch(url, request, cacheKey, workerURL)
  } else if (url.pathname === '/robots.txt') {
    response = new Response('User-agent: *\nDisallow: /', {
      headers: { 'Content-Type': 'text/plain' },
    })
  } else {
    response = await handleSite(url, request, cacheKey, workerURL)
  }
  return response
}
class ReplaceHostAttributeRewriter {
  attributeName: string
  newHost: string
  oldHost: string
  constructor(attributeName: string, oldHost: string, newHost: string) {
    this.attributeName = attributeName
    this.oldHost = oldHost
    this.newHost = newHost
  }
  element(element: Element) {
    const attribute = element.getAttribute(this.attributeName)
    if (attribute) {
      element.setAttribute(
        this.attributeName,
        attribute.replace(this.oldHost, this.newHost),
      )
    }
  }
}

class AttributeRewriter {
  attributeName: string
  attributeValue: string

  constructor(attributeName: string, attributeValue: string) {
    this.attributeName = attributeName
    this.attributeValue = attributeValue
  }
  element(element: Element) {
    element.setAttribute(this.attributeName, this.attributeValue)
  }
}

async function handleSite(
  url: URL,
  request: Request,
  cacheKey: string,
  workerURL: URL,
): Promise<Response> {
  console.debug('Site')

  let response: Response

  const language = url.searchParams.get('lang')
  if (language) {
    url.pathname = `/${language}/${url.pathname}`
    url.searchParams.delete('lang')
    return new Response(null, {
      status: 302,
      headers: { location: url.toString() },
    })
  }

  const origin_url = WEBSITE_URL
  origin_url.pathname = url.pathname
  origin_url.search = url.search
  request = new Request(origin_url.toString(), request)

  response = await secureFetch(request, {
    cacheKey,
    cacheTtl: 86400,
    resolveOverride: WEBSITE_ORIGIN_RESOLVE_OVERRIDE,
  })
  response = new Response(response.body, response)

  // Default to gzip files
  // https://community.cloudflare.com/t/worker-doesnt-return-gzip-brotli-compressed-data/337644/3
  response.headers.set(
    'Content-Encoding',
    response.headers.get('Content-Encoding') || 'gzip',
  )

  if (response.status === 301 || response.status === 302) {
    let location = response.headers.get('location')
    if (location) {
      location = location.replace(WEBSITE_STAGING_URL.origin, workerURL.origin)
      response.headers.set('location', location)
    }
  } else if (response.headers.get('content-type')?.includes('text/html')) {
    // don't modify non-html responses such that they can be cached
    // Replace repo url to the worker
    const originRewriter = new HTMLRewriter()
      .on(
        'a',
        new ReplaceHostAttributeRewriter(
          'href',
          WEBSITE_STAGING_URL.origin,
          workerURL.origin,
        ),
      )
      .on(
        'img',
        new ReplaceHostAttributeRewriter(
          'src',
          WEBSITE_STAGING_URL.origin,
          workerURL.origin,
        ),
      )
      .on(
        'a',
        new ReplaceHostAttributeRewriter(
          'href',
          WEBSITE_URL.origin,
          workerURL.origin,
        ),
      )
      .on(
        'img',
        new ReplaceHostAttributeRewriter(
          'src',
          WEBSITE_URL.origin,
          workerURL.origin,
        ),
      )
      .on(
        'form',
        new ReplaceHostAttributeRewriter(
          'action',
          'https://search.f-droid.org',
          workerURL.origin + '/search',
        ),
      )
      .on(
        'meta',
        new ReplaceHostAttributeRewriter(
          'content',
          WEBSITE_URL.origin,
          workerURL.origin,
        ),
      )
      // don't lazy load large visible images that impacts LCP: https://web.dev/lcp-lazy-loading/
      .on('.main-content img', new AttributeRewriter('loading', 'lazy'))
      .on(
        '.main-content img[src^="/assets/phone-frame_"]',
        new AttributeRewriter('loading', 'eager'),
      )
      // defer non-critical script execution
      .on(
        'script[src^="/assets/sidebar-lang_"]',
        new AttributeRewriter('defer', 'true'),
      )
      .on(
        'script[src^="/assets/donate_"]',
        new AttributeRewriter('defer', 'true'),
      )
      // Preload
      .on(
        'html',
        new AddPreloadRewriter(
          '<link rel="preload" href="/css/main.css" as="style" />',
        ),
      )

    // addEventListener('fetch', (event) => {
    //   event.respondWith(handleRequest(event.request))
    // })
    // // Repo has mirrors
    // const mirrorRewriter = new HTMLRewriter()
    //   .on('a', new AttributeRewriter('href', MIRROR_HOST, WORKER_HOST))
    //   .on('img', new AttributeRewriter('src', MIRROR_HOST, WORKER_HOST))
    // addEventListener('fetch', (event) => {
    //   event.respondWith(handleRequest(event.request))
    // })
    response = originRewriter.transform(response)

    // CSP
    response.headers.set(
      'content-security-policy',
      "default-src 'none';base-uri 'self';block-all-mixed-content;connect-src 'self';font-src 'self';form-action 'self' https://search.f-droid.org;frame-ancestors 'self';img-src 'self' https://f-droid.org https://fdroid.gitlab.io;manifest-src 'self';media-src 'self';script-src 'self' https://ajax.cloudflare.com/cdn-cgi/scripts/;style-src 'self' 'unsafe-inline';",
    )

    // // disable tests only on production site
    // if (ENVIRONMENT !== 'production') {
    //   // replace with meilisearch searchbar
    //   response = await addMeilisearchSearchBar(response)
    // }
  }
  return response
}

async function handleRepo(
  url: URL,
  request: Request,
  cacheKey: string,
  workerURL: URL,
): Promise<Response> {
  console.debug('Repo')

  let response: Response

  const continent = request.cf?.continent ?? 'EU'
  const country = request.cf?.country ?? 'DE'
  const colo = request.cf?.colo ?? 'DFW'

  if (url.pathname === '/repo') {
    const init = { status: 301, headers: { Location: '/repo/' } }
    response = new Response(null, init)
  } else {
    const mirror_url = getMirror(continent, country, colo)
    console.debug(`mirror_url: ${mirror_url}`)

    mirror_url.pathname = mirror_url.pathname + url.pathname
    console.debug('Mirror: ' + mirror_url)

    request = new Request(mirror_url.toString(), request)
    // console.debug(url)

    if (url.pathname.match(INDEX_PATH_REGEX)) {
      console.debug('Index')
      response = await secureFetch(
        request,
        { cacheKey },
        REPO_MIRRORS.get('default'),
      )

      // replaces the website url only if it's xml or html
      if (
        response.headers.get('content-type') === 'application/xml' ||
        response.headers.get('content-type') === 'application/html'
      ) {
        let body = await response.text()
        body = body.replaceAll(WEBSITE_URL.origin, workerURL.origin)
        response = new Response(body, response)
      }
    } else {
      // response = await secureFetch(request, {
      //   cacheKey,
      // })
      response = await fetch(request.url)

      console.log('response.url: ' + response.url)

      console.log('response.headers')
      response.headers.forEach((value, key) => {
        console.log(`${key}: ${value}`)
      })

      const suffix = url.pathname.match(SUFFIX_REGEX)
      if (suffix && suffix?.length > 0) {
        response = new Response(response.body, response)
        const contentType = CONTENT_TYPES.get(suffix[0])
        if (contentType) {
          response.headers.set('Content-Type', contentType)
        }
      }
    }
  }
  return response
}

async function handleSearch(
  url: URL,
  request: Request,
  cacheKey: string,
  workerURL: URL,
): Promise<Response> {
  console.debug('Search')

  const origin_url = SEARCH_ORIGIN
  origin_url.pathname = url.pathname.replace(SEARCH_PATH_REGEX, '')
  origin_url.search = url.search

  console.debug(origin_url)

  // If it is, proxy request to that third party origin
  request = new Request(origin_url.toString(), request)
  let response = await secureFetch(request, { cacheKey })

  if (response.headers.get('content-type')?.includes('text/html')) {
    // don't modify non-html responses such that they can be cached
    const originRewriter = new HTMLRewriter()
      .on(
        'a',
        new ReplaceHostAttributeRewriter(
          'href',
          WEBSITE_URL.origin,
          workerURL.origin,
        ),
      )
      .on(
        'img',
        new ReplaceHostAttributeRewriter(
          'src',
          WEBSITE_URL.origin,
          workerURL.origin,
        ),
      )
      .on(
        'img',
        new ReplaceHostAttributeRewriter(
          'src',
          'https://fdroid.tetaneutral.net/fdroid',
          workerURL.origin,
        ),
      )
      .on(
        'img',
        new ReplaceHostAttributeRewriter(
          'src',
          'https://ftp.fau.de/fdroid',
          workerURL.origin,
        ),
      )
      .on(
        'img',
        new ReplaceHostAttributeRewriter(
          'src',
          'https://f-droid.org',
          workerURL.origin,
        ),
      )
      .on(
        'link',
        new ReplaceHostAttributeRewriter('href', '/static', '/search/static'),
      )
      .on(
        'img',
        new ReplaceHostAttributeRewriter('src', '/static', '/search/static'),
      )
      // don't lazy load large visible images that impacts LCP: https://web.dev/lcp-lazy-loading/
      .on('.main-content img', new AttributeRewriter('loading', 'lazy'))
      // Preload
      .on(
        'html',
        new AddPreloadRewriter(
          '<link rel="preload" href="/search/static/fdroid_website_search.css" as="style" />',
        ),
      )

    response = originRewriter.transform(response)

    response = new Response(response.body, response)

    // CSP
    response.headers.set(
      'content-security-policy',
      "default-src 'none'; img-src 'self' https:; script-src 'self' https://ajax.cloudflare.com/cdn-cgi/scripts/; style-src 'self' 'unsafe-inline'; font-src 'self'; frame-ancestors 'none'; base-uri 'none'; form-action 'self'",
    )
  } else if (
    workerURL.pathname === '/search/static/fdroid_website_search.css'
  ) {
    let body = await response.text()
    body = body.replaceAll('url(/static/', 'url(/search/static/')
    response = new Response(body, response)
  }

  return response
}

async function secureFetch(
  request: Request,
  cfProperties: RequestInitCfProperties,
  fallbackUrl?: URL,
): Promise<Response> {
  console.log(request.url)

  let response = await fetchCf(request, cfProperties)
  // Fallback to the slow main site if it fails
  if (fallbackUrl && response.status >= 400) {
    request = new Request(fallbackUrl.toString(), request)
    response = await fetchCf(request, cfProperties)
  }
  return response
}

async function fetchCf(
  request: Request,
  cfProperties: RequestInitCfProperties,
): Promise<Response> {
  return fetch(request, {
    cf: {
      ...defaultCfProperties,
      ...cfProperties,
    },
  })
}

async function addMeilisearchSearchBar(response: Response) {
  return new HTMLRewriter()
    .on('.search-form', new MeilisearchSearchBarGenerator())
    .on('.search-form > input.material-button', new SearchButtonNoScriptAdder())
    .on('head', new DocsSearchBarScriptImporter())
    .transform(response)
}

class AddPreloadRewriter {
  html: Content
  constructor(html: string) {
    this.html = html
  }
  element(head: Element) {
    head.prepend(this.html, {
      // inject it as plain HTML
      html: true,
    })
  }
}

class DocsSearchBarScriptImporter {
  element(head: Element) {
    head.append(
      `<link
    rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/docs-searchbar.js@${MEILISEARCH_DOCS_SEARCHBAR_VERSION}/dist/cdn/docs-searchbar.min.css"
  />`,
      {
        // inject it as plain HTML
        html: true,
      },
    )
  }
}

class MeilisearchSearchBarGenerator {
  element(searchbar: Element) {
    searchbar.attributes
    searchbar.append(
      `
    <script src="https://cdn.jsdelivr.net/npm/docs-searchbar.js@${MEILISEARCH_DOCS_SEARCHBAR_VERSION}/dist/cdn/docs-searchbar.min.js"></script>
    <script>
        docsSearchBar({
            hostUrl: '${MEILISEARCH_HOST_URL}',
            apiKey: '${MEILISEARCH_API_KEY}',
            indexUid: 'app_details',
            inputSelector: '.search-input',
            debug: true, // Set debug to true if you want to inspect the dropdown,
            meilisearchOptions: {
                limit: 10,
            },
            enableDarkMode: 'auto'
        })
    </script>`,
      {
        // inject it as plain HTML
        html: true,
      },
    )
  }
}

class SearchButtonNoScriptAdder {
  element(searchButton: Element) {
    searchButton.replace(
      `<noscript><input type="submit" value="Search" class="material-button"></noscript>`,
      { html: true },
    )
  }
}
